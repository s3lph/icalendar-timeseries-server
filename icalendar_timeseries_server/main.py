import sys
import logging

import bottle

from icalendar_timeseries_server.cal import start_scrape_calendar
from icalendar_timeseries_server.config import load_config, load_default_config, get_config

# Contains decorated bottle handler function for /api/v1/query
# noinspection PyUnresolvedReferences
from icalendar_timeseries_server.api import prometheus_api


def main():
    # Set up logger
    log_handler = logging.StreamHandler()
    log_handler.setFormatter(logging.Formatter(
        '%(asctime)s %(filename)s:%(lineno)d(%(funcName)s) [%(levelname)s]: %(message)s'))
    logging.getLogger().addHandler(log_handler)

    # Load configuration
    config = get_config()
    try:
        if len(sys.argv) == 1:
            load_default_config()
        elif len(sys.argv) == 2:
            load_config(sys.argv[1])
        else:
            logging.log(logging.FATAL, f'Can only read one config file, got "{" ".join(sys.argv[1:])}"')
            exit(1)
        # Re-fetch config after parsing
        config = get_config()
    except BaseException:
        logging.fatal('Could not parse configuration file')
        exit(1)

    # Schedule calendar scraping in the background
    for calname in config.calendars.keys():
        start_scrape_calendar(calname, config.calendars[calname])

    # Start the Bottle HTTP server
    bottle.run(host=config.addr, port=get_config().port)


if __name__ == '__main__':
    main()
