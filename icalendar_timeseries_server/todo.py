from typing import Any, Dict, List

import icalendar
import jinja2
from datetime import datetime, date, timedelta

from icalendar_timeseries_server.config import get_config, get_jenv
from icalendar_timeseries_server.query import Metric

_ATTRIBUTES: List[str] = [
    'class',
    'description',
    'geo',
    'location',
    'organizer',
    'percent-complete',
    'priority',
    'status',
    'summary',
    'url',
    'attach'
]


class Todo(Metric):

    def __init__(self, cname: str, todo: icalendar.cal.Todo, start: datetime, end: datetime):
        self.calendar: str = cname
        self.start = start
        due = todo.get('due', None)
        if due:
            if isinstance(due.dt, datetime):
                self.due = due.dt
            elif isinstance(due.dt, date):
                self.due = datetime.combine(due.dt, datetime.min.time())
                self.due = self.due.replace(tzinfo=get_config().tz)
        else:
            self.due = None
        # self.attributes: Dict[str, str] = dict()
        attributes: Dict[str, str] = dict()
        tmp: Dict[str, Any] = {
            'calendar': cname,
            'start': start,
            'end': end
        }
        if self.due:
            tmp['due'] = str(self.due)
        for attr in _ATTRIBUTES:
            tmp[attr] = todo.get(attr, '')
        substitution_keys = set(_ATTRIBUTES)
        substitution_keys.update(tmp.keys())
        substitution_keys.update(get_config().key_replace.keys())
        substitution_keys.update(get_config().value_replace.keys())
        for attr in substitution_keys:
            newkey: str = get_config().key_replace.get(attr, attr)
            value: str = tmp.get(attr, '')
            newval_template: str = get_config().value_replace.get(attr, str(value))
            jtemplate: jinja2.Template = get_jenv().from_string(newval_template)
            newvalue: str = jtemplate.render(**tmp)
            attributes[newkey] = newvalue
        self.uid: str = f'{cname}-{start.strftime("%Y%m%dT%H%M%S%Z")}'
        self.priority = todo.get('priority', '0')
        super().__init__('todo', attributes)

    def serialize(self) -> Dict[str, Any]:
        todo: Dict[str, Any] = {
            'metric': {
                '__name__': 'todo',
                'calendar': self.calendar
            },
            'value': [
                self.start.timestamp(),
                "1"
            ]
        }
        todo['metric'].update(self._labels)
        return todo
