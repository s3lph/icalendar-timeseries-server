---
image: s3lph/icalendar-timeseries-server-ci:20190820-01

stages:
- test
- build
- release
- upload



before_script:
- export ITS_VERSION=$(python -c 'import icalendar_timeseries_server; print(icalendar_timeseries_server.__version__)')



test:
  stage: test
  script:
  - pip3 install -e .
  - sudo -u its python3 -m coverage run --rcfile=setup.cfg -m unittest discover icalendar_timeseries_server
  - sudo -u its python3 -m coverage combine
  - sudo -u its python3 -m coverage report --rcfile=setup.cfg

codestyle:
  stage: test
  script:
  - pip3 install -e .
  - sudo -u its pycodestyle icalendar_timeseries_server



build_wheel:
  stage: build
  script:
  - pip3 install -e .
  - python3 setup.py egg_info bdist_wheel
  - cd dist
  - sha256sum *.whl > SHA256SUMS
  artifacts:
    paths:
    - "dist/*.whl"
    - dist/SHA256SUMS
  only:
  - tags

build_debian:
  stage: build
  script:
  # The Python package name provided by the python3-magic Debian package is "python-magic" rather than "file-magic".
  - sed -re 's/file-magic/python-magic/' -i setup.py
  - echo -n > package/debian/icalendar-timeseries-server/usr/share/doc/icalendar-timeseries-server/changelog
  - |
    cat > package/debian/icalendar-timeseries-server/DEBIAN/control <<EOF
    Package: icalendar-timeseries-server
    Version: ${ITS_VERSION}
    Maintainer: ${PACKAGE_AUTHOR}
    Section: web
    Priority: optional
    Architecture: all
    Depends: python3 (>= 3.7), python3-jinja2, python3-bottle, python3-dateutil, python3-icalendar, python3-isodate
    Description: Scrape iCalendar endpoints and present their data in a
     timeseries format.  A small service that scrapes iCalendar files
     served over HTTP, parses their contents and returns a timeseries
     format compatible to the /api/v1/query API endpoint of a Prometheus
     server. This allows e.g. a Grafana administrator to add a Prometheus
     data source pointing at this server, returning the events in the
     calendars in the event metric.
    EOF
  - |
    for version in $(cat CHANGELOG.md | grep '<!-- BEGIN CHANGES' | cut -d ' ' -f 4); do
      echo "icalendar-timeseries-server (${version}-1); urgency=medium\n" >> package/debian/icalendar-timeseries-server/usr/share/doc/icalendar-timeseries-server/changelog
      cat CHANGELOG.md | grep -A 1000 "<"'!'"-- BEGIN CHANGES ${version} -->" | grep -B 1000 "<"'!'"-- END CHANGES ${version} -->" | tail -n +2 | head -n -1 | sed -re 's/^-/  */g' >> package/debian/icalendar-timeseries-server/usr/share/doc/icalendar-timeseries-server/changelog
      echo "\n -- ${PACKAGE_AUTHOR}  $(date -R)\n" >> package/debian/icalendar-timeseries-server/usr/share/doc/icalendar-timeseries-server/changelog
    done
  - gzip -9n package/debian/icalendar-timeseries-server/usr/share/doc/icalendar-timeseries-server/changelog
  - python3.7 setup.py egg_info install --root=package/debian/icalendar-timeseries-server/ --prefix=/usr --optimize=1
  - cd package/debian
  - mkdir -p icalendar-timeseries-server/usr/lib/python3/dist-packages/
  - rsync -a icalendar-timeseries-server/usr/lib/python3.7/site-packages/ icalendar-timeseries-server/usr/lib/python3/dist-packages/
  - rm -rf icalendar-timeseries-server/usr/lib/python3.7/
  - find icalendar-timeseries-server/usr/lib/python3/dist-packages -name __pycache__ -exec rm -r {} \; 2>/dev/null || true
  - find icalendar-timeseries-server/usr/lib/python3/dist-packages -name '*.pyc' -exec rm {} \;
  - mv icalendar-timeseries-server/usr/bin/icalendar-timeseries-server icalendar-timeseries-server/usr/lib/icalendar-timeseries-server/icalendar-timeseries-server
  - rm -rf icalendar-timeseries-server/usr/bin
  - sed -re 's$#!/usr/local/bin/python3.7$#!/usr/bin/python3$' -i icalendar-timeseries-server/usr/lib/icalendar-timeseries-server/icalendar-timeseries-server
  - find icalendar-timeseries-server -type f -exec chmod 0644 {} \;
  - find icalendar-timeseries-server -type d -exec chmod 755 {} \;
  - find icalendar-timeseries-server -type f -name .gitkeep -delete
  - chmod +x icalendar-timeseries-server/usr/lib/icalendar-timeseries-server/icalendar-timeseries-server icalendar-timeseries-server/DEBIAN/postinst icalendar-timeseries-server/DEBIAN/prerm icalendar-timeseries-server/DEBIAN/postrm
  - dpkg-deb --build icalendar-timeseries-server
  - mv icalendar-timeseries-server.deb "icalendar-timeseries-server_${ITS_VERSION}-1_all.deb"
  - sudo -u nobody lintian "icalendar-timeseries-server_${ITS_VERSION}-1_all.deb"
  - sha256sum *.deb > SHA256SUMS
  artifacts:
    paths:
    - "package/debian/*.deb"
    - package/debian/SHA256SUMS
  only:
  - tags



release:
  stage: release
  script:
  - python package/release.py
  only:
  - tags



repo:
  stage: upload
  trigger: s3lph/custom-packages
  variables:
    MULTIPROJECT_TRIGGER_JOBNAME: icalendar-timeseries-server
  only:
  - tags
