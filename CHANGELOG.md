# iCalendar Timeseries Server Changelog


<!-- BEGIN RELEASE v0.6.1 -->
## Version 0.6.1

### Changes

<!-- BEGIN CHANGES 0.6.1 -->
- Same fix, but for todo as well as events.

<!-- END CHANGES 0.6.1 -->

<!-- END RELEASE v0.6.1 -->


<!-- BEGIN RELEASE v0.6 -->
## Version 0.6

### Changes

<!-- BEGIN CHANGES 0.6 -->
- Fix: A specific API field has to be a string rather than float, and recent Grafana versions validate this.

<!-- END CHANGES 0.6 -->

<!-- END RELEASE v0.6 -->


<!-- BEGIN RELEASE v0.5 -->
## Version 0.5

### Changes

<!-- BEGIN CHANGES 0.5 -->
- Retry calendar scraping with exponential backoff.
<!-- END CHANGES 0.5 -->

<!-- END RELEASE v0.5 -->


<!-- BEGIN RELEASE v0.4.1 -->
## Version 0.4.1

### Changes

<!-- BEGIN CHANGES 0.4.1 -->
- Fix todo sorting by due date.
- Update README regarding `todo` time series.
<!-- END CHANGES 0.4.1 -->

<!-- END RELEASE v0.4.1 -->


<!-- BEGIN RELEASE v0.4.0 -->
## Version 0.4.0

### Changes

<!-- BEGIN CHANGES 0.4.0 -->
- VTODO components are exported in a second time series, `todo` . Todo recurrence is not supported yet though.
<!-- END CHANGES 0.4.0 -->

<!-- END RELEASE v0.4.0 -->


<!-- BEGIN RELEASE v0.3.3 -->
## Version 0.3.3

### Changes

<!-- BEGIN CHANGES 0.3.3 -->
- Fix type confusion bug in recurring events
- Remove pytz dependency in favor of dateutil.tz
<!-- END CHANGES 0.3.3 -->

<!-- END RELEASE v0.3.3 -->


<!-- BEGIN RELEASE v0.3.2 -->
## Version 0.3.2

### Changes

<!-- BEGIN CHANGES 0.3.2 -->
- Fix Debian package build process
<!-- END CHANGES 0.3.2 -->

<!-- END RELEASE v0.3.2 -->


<!-- BEGIN RELEASE v0.3.1 -->
## Version 0.3.1

### Changes

<!-- BEGIN CHANGES 0.3.1 -->
- Bump Version Number
<!-- END CHANGES 0.3.1 -->

<!-- END RELEASE v0.3.1 -->


<!-- BEGIN RELEASE v0.3 -->
## Version 0.3

### Changes

<!-- BEGIN CHANGES 0.3 -->
- Replace print statements by proper logging
- Fix: Ensure scrape interval is positive
- Fix: Keep showing events that already started, but have not finished yet
<!-- END CHANGES 0.3 -->

<!-- END RELEASE v0.3 -->


<!-- BEGIN RELEASE v0.2 -->
## Version 0.2

### Changes

<!-- BEGIN CHANGES 0.2 -->
- Scrape intervals are now configured per calendar
- Calendar scraping now happens in the background
<!-- END CHANGES 0.2 -->

<!-- END RELEASE v0.2 -->


<!-- BEGIN RELEASE v0.1 -->
## Version 0.1

First pre-1.0 release of iCalendar Timeseries Server.

### Changes

<!-- BEGIN CHANGES 0.1 -->
- Configure multiple scrape sources
- Authorization: HTTP Basic Auth or TLS Client Certificates
- Offer API endpoints /api/v1/query and /api/v1/query_range
- Support simple PromQL label filters
<!-- END CHANGES 0.1 -->

<!-- END RELEASE v0.1 -->
