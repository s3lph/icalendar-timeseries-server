#!/usr/bin/env python3

from setuptools import setup, find_packages

from icalendar_timeseries_server import __version__

setup(
    name='icalendar_timeseries_server',
    version=__version__,
    author='s3lph',
    author_email='',
    description='',
    license='MIT',
    keywords='ical,icalendar,timeseries,prometheus,grafana',
    url='https://gitlab.com/s3lph/icalendar-timeseries-server',
    packages=find_packages(exclude=['*.test']),
    long_description='',
    python_requires='>=3.6',
    install_requires=[
        'bottle',
        'python-dateutil>=2.8',
        'icalendar',
        'isodate',
        'jinja2'
    ],
    entry_points={
        'console_scripts': [
            'icalendar-timeseries-server = icalendar_timeseries_server:main'
        ]
    },
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Framework :: Bottle',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: MIT License',
        'Topic :: System :: Monitoring'
    ],
)
